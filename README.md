Lightweight-PHP
====================

This framework was developed out of frustration not understanding what's going on "under the hood" of PHP frameworks. The purpose is to keep it simple while still usefull. It is a subset of of CodeIgniter. 

The framework was developed writing an article which also explains how it was built: http://colmsjo.com/Shrinking-codeigniter-details/

If you want a full featured PHP framework, check out http://codeigniter.com/
